# Translating application for text to hand signs

The application lets you log in with a username and translate text to hand signs to help you communicate with deaf or 
hard of hearing people who use sign language. Please read the 

## Heads up
The database <translator.json> which is in the root folder should be run on port 8000 with the following command while in the root folder:

`json-server --watch translator.json --port 8000`

If you fail to do this, the application won't work properly.

## Pages
- Login ('base_url/')
    - The login page features a kind robot, and an input field. Here you can type in any username you'd like, or try 
    the already created profile **Timmy**. The login page will not display if you're already logged in.
      
- Translator ('base_url/translate')
    - Once logged in, the user is sent to the translator page. You will now see two new buttons in the navigation bar,
    one which takes you to you profile page, and one for logging out. There is an input field where the user can
      write the desired word or text for translating. Clicking the arrow button in the input field triggers two actions,
      one which displays the hand signs in the output box, and one which saves the written text to the user.
      
- Profile ('base_url/profile/:id')
    - The profile page is accessed by using the button in the navigation bar and shows the currently logged-in user 
      and 10 most recent translations. If the user have none, no translations will be shown. It also features a button which takes the user back to the translator.
      
## In progress

- Have a _clear_-button on the profile page that deletes the translations for the page and marks it as deleted in the 
  database
  
### Possible updates

- Should add a password check when logging in.
- Should optimize the database section for a users translations to be an array of strings and not objects.
- Should make the logout function use redux and not force a reload of the page.
- Add numbering to the 10 most recent translations a user has done.

