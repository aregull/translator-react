import './App.css'
import {
    BrowserRouter,
    Switch,
    Route,
    Redirect
} from 'react-router-dom'
import NotFound from "./components/NotFound/NotFound";
import Translator from "./components/Translator/Translator"
import Welcome from "./components/WelcomePage/Welcome"
import Header from "./components/Header/Header";
import Profile from "./components/Profile/Profile"

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Header/>
                <Switch>
                    <Route path="/" exact component={Welcome}/>
                    <Route path="/translate" component={Translator} />
                    <Route path="/profiles/:id" component={Profile}/>
                    <Route path="*" component={NotFound}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
