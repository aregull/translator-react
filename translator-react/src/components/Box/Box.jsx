import styles from './Box.module.css';

{/* Box is the white with purple footer element login and translate pages.
    It receives the children elements and displays them in its main area.*/
}

const Box = ({children}) => {

    return (
        <div className={styles.Box}>
            <div className={styles.BoxMain}> {children} </div>
            <div className={styles.BoxFooter}></div>
        </div>
    )
}

export default Box