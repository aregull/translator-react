import styles from './Navbar.module.css'
import Navbar from "./Navbar";

{/*Header holds the navigation bar*/
}

const Header = () => {

    return (
        <>
            <header className={styles.Header}>
                <Navbar/>
            </header>
        </>

    )

}

export default Header