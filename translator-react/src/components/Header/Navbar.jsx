import styles from './Navbar.module.css'
import {useSelector} from "react-redux";
import {useHistory} from 'react-router-dom'
import {useState} from "react";

{
    /*The navigation bar is held by Header and it shows the title and, if logged-in, the profile and logout buttons.
    When either the profile or logout button is pressed, it triggers the function bound to the button.*/
}

const Navbar = () => {

    const {loggedIn, name, id} = useSelector(state => state.sessionReducer)
    const history = useHistory();

    const goToProfile = () => {
        history.push("/profiles/" + id)
    }

    const logOutButton = () => {
        localStorage.clear();
        history.push('/')
        window.location.reload();
    }

    return (
        <header className="">
            <nav className={styles.Navbar}>
                <h1 className={styles.NavbarTitle}>Lost in Translation</h1>

                {loggedIn &&
                <div className={styles.NavProfileButtons}>
                    <div className={styles.NavProfile} onClick={goToProfile}>
                        <span>Hello {name} </span>
                        <span className="material-icons">face</span>
                    </div>
                    <div className={styles.NavProfile} onClick={logOutButton}>
                        <span>Log out</span>
                        <span className="material-icons"> highlight_off</span>
                    </div>
                </div>
                }

            </nav>

        </header>
    )
}

export default Navbar