import AppContainer from "../../hoc/AppContainer";
import {useState} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {loginAttemptAction} from "../../store/actions/loginActions";
import {
    Redirect
} from 'react-router-dom'

{
    /* The login page handles logging in with a username. If the user already is logged in, they cannot access this page
    * and will be forwared to the translator. Upon entering their name, it will fire off the login API */
}

const Login = () => {


    const dispatch = useDispatch()
    const {loginAttempting} = useSelector(state => state.loginReducer)
    const {loggedIn} = useSelector(state => state.sessionReducer)

    const [credentials, setCredentials] = useState({
        name: '',
        translation: []
    })

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })
    }

    const onFormSubmit = event => {
        event.preventDefault()
        if (credentials.name === "") {
            alert("Name cannot be empty")
            return;
        }
        dispatch(loginAttemptAction(credentials))
    }

    return (
        <>
            {loggedIn && <Redirect to="/translate"/>}
            {!loggedIn &&
            <AppContainer>
                <form className="mt-3" onSubmit={onFormSubmit}>
                    <div className="input-group-text rounded-pill bg-light mb-3">
                        <span className="material-icons ">keyboard</span>

                        <input id="name" type="text"
                               placeholder="Enter username, or try 'Timmy'"
                               className="form-control rounded-pill border-0"
                               onChange={onInputChange}/>
                        <button className="material-icons" type="submit">arrow_forward</button>
                    </div>
                </form>

                {loginAttempting &&
                <h3>Trying to log in</h3>
                }
            </AppContainer>
            }
        </>

    )
}

export default Login