{
    /* This API is used when logging in and handles two situations. Getting a user or creating a new user.
    * If a user tries to log in and has previously been entered to the database, they will access the same user.
    * If the name is unique, a new profile will be made.*/
}


export const LoginAPI = {
    async login(credentials) {

        const existingUser = await checkUser(credentials.name)

        if (existingUser.length) {
            return new Promise(resolve => resolve(existingUser.pop()))
        }

        return fetch('http://localhost:8000/profiles', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials) //IS CREDS full profile??
        })
            .then(async (response) => {
                if (!response.ok) {
                    const {error = 'An unknown error occured'} = await response.json()
                    throw new Error(error)
                }
                return response.json()
            })
    }
}

async function checkUser(name) {
    return fetch(`http://localhost:8000/profiles?name=` + name)
        .then(response => response.json())
}