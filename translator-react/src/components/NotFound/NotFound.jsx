import {Link} from 'react-router-dom'
import AppContainer from "../../hoc/AppContainer";

{
    /* If you try to enter an invalid url, this page will be shown. */
}

const NotFound = () => {


    return (
        <AppContainer>
            <main>
                <h1>Hey, you're lost</h1>
                <h2>This page does not exist</h2>
                <Link to="/">Take me home</Link>
            </main>
        </AppContainer>
    )
}

export default NotFound