import AppContainer from "../../hoc/AppContainer";
import {useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import styles from './Profile.module.css'

{
    /* The profile page will display a welcome message to the currently logged-in user, and its 10 most recent
    * translations. A button to take the user to the translator will be present directly over the translations. */
}


const Profile = () => {

    const {name, translation} = useSelector(state => state.sessionReducer)
    let history = useHistory();

    const goToTranslator = () => {

        history.push("/translate")
    }

    return (
        <>
            <main className={styles.Welcome}>
                <div className={styles.Content}>
                    <img src="/images/Logo-Hello.png" className={styles.Image} alt="Welcoming robot"/>
                    <div className="d-flex flex-column">
                        <h1>Welcome to your profile, {name}</h1>
                    </div>
                </div>
            </main>
            <AppContainer>
                {translation &&
                <section className={styles.NoTranslations}>
                    <button onClick={goToTranslator} className={styles.TranslateButtons}>Go Translate!</button>
                    <ul>
                        {translation.map(trans => (
                            <li key={trans.translation}>{trans.translation}</li>
                        ))}
                    </ul>
                </section>}
            </AppContainer>
        </>
    )

}

export default Profile