{
    /* This API will PATCH a user-object, which will only alter the users "translation" property */
}


export const ProfileAPI = {

    addTranslation(profile) {
        return fetch('http://localhost:8000/profiles/' + profile.id, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({translation: profile.translation})
        }).then(data => {
            let tempArray = []
            for (let i = 0; i < data.results.length; i++) {
                tempArray.push(data.results[i].translation)
            }

            this.setState({
                translation: tempArray
            })
        })
            .then(async (response) => {
                if (!response.ok) {
                    const {error = 'An unknown error occured'} = await response.json()
                    throw new Error(error)
                }
                return response.json()
            })
    }
}