{
    /* Takes the text the user wants to translate and maps it and alters it to an image string, which will
    * get the image from the public folder and display it to the user. */
}

const TranslationOutput = props => {

    const {text} = props;

    return (
        <>
            {
                text.split('').map((char, index) => (
                    <img src={`./images/${char}.png`} alt={"Sign language symbol"} key={index}/>

                ))
            }
        </>)

}
export default TranslationOutput