import Box from '../Box/Box'
import AppContainer from "../../hoc/AppContainer";
import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
import styles from './Translator.module.css'
import TranslationOutput from "./TranslationOutput";
import {translateAttemptAction} from "../../store/actions/profileActions";
import {Redirect} from "react-router-dom";

{
    /* The translator does two things, call the API to update the users translations, and send the text that should
    * be displayed to the TranslationOutput. If the users amount of translations equals to 10, it will shift the array
    * and delete the oldest record, then the API will add in the new value. */
}

const Translator = () => {

    const {id, translation, loggedIn} = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()
    const [textToTranslate, setTextToTranslate] = useState('')
    const [signLanguageText, setSignLanguageText] = useState('')


    const onInputChange = event => {
        setTextToTranslate({
            ...textToTranslate,
            [event.target.id]: event.target.value
        })
    }

    const displayTranslation = () => {
        let text = textToTranslate.translation.replace(/[^a-zA-Z]/g, '')
        text = text.toLowerCase()
        setSignLanguageText(text)
    }

    const onFormSubmit = event => {
        event.preventDefault()
        displayTranslation()
        if (translation.length === 10) {
            translation.shift()
        }
        translation.push(textToTranslate);
        dispatch(translateAttemptAction({
            id: id,
            translation: translation
        }))
    }

    return (
        <> {!loggedIn && <Redirect to="/"/>}
            {loggedIn &&
            <>
                <main className={styles.Input} onSubmit={onFormSubmit}>
                    <div className="input-group-text rounded-pill bg-light container">
                        <span className="material-icons ">keyboard</span>

                        <input id="translation" type="text"
                               placeholder="What do you want to translate?"
                               className="form-control rounded-pill border-0"
                               onChange={onInputChange}/>
                        <span className="material-icons" type="submit" onClick={onFormSubmit}>arrow_forward</span>
                    </div>
                </main>
                <AppContainer>
                    <Box>
                        <TranslationOutput text={signLanguageText}/>
                    </Box>

                </AppContainer>
            </>}
        </>

    )
}

export default Translator