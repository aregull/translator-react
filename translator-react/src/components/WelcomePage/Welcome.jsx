import styles from './WelcomePage.module.css'
import Login from "../Login/Login";
import Box from "../Box/Box";
import AppContainer from "../../hoc/AppContainer";

{
    /* The welcome page is the page responsible of showing the login input element. It will also handle the display
    * of the welcoming robot and the title of the page. */
}

const Welcome = () => {

    return (
        <>
            <main className={styles.Welcome}>
                <div className={styles.Content}>
                    <img src="/images/Logo.png" className={styles.Image} alt="Welcoming robot"
                         style={{
                             backgroundImage: `url('./images/Splash.svg')`,
                             backgroundRepeat: "no-repeat",
                             backgroundSize: "contain"
                         }}/>

                    <div className="d-flex flex-column">
                        <h1>Lost in Translation</h1>
                        <h2>Get started</h2>
                    </div>
                </div>
            </main>
            <AppContainer>
                <Box>
                    <Login/>
                </Box>
            </AppContainer>
        </>
    )
}

export default Welcome