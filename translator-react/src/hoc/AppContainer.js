
{
    /* AppContainer makes use of the bootstrap container class which centers its content. */
}

const AppContainer = ({ children }) => {
    return (

        <div className="container">{children}</div>

    )
}


export default AppContainer