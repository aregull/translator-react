export const PROFILE_TRANSLATION_ATTEMPTING = '[translating] ATTEMPT'
export const PROFILE_TRANSLATION_SUCCESS = '[translating] SUCCESS'
export const PROFILE_TRANSLATION_ERROR = '[translating] ERROR'

export const translateAttemptAction = profile => ({
    type: PROFILE_TRANSLATION_ATTEMPTING,
    payload: profile
})

export const translateSuccessAction = profile => ({
    type: PROFILE_TRANSLATION_SUCCESS,
    payload: profile.translation
})

export const translateErrorAction = error => ({
    type: PROFILE_TRANSLATION_ERROR,
    payload: error
})