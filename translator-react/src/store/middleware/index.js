import { loginMiddleware } from './loginMiddleware'
import {applyMiddleware} from "redux";
import {sessionMiddleware} from "./sessionMiddleware";
import {profileMiddleware} from "./profileMiddleware";

export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    profileMiddleware,
)
