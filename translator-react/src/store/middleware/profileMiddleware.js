import {
    PROFILE_TRANSLATION_ATTEMPTING,
    PROFILE_TRANSLATION_SUCCESS,
    translateSuccessAction,
    translateAttemptAction, translateErrorAction
} from '../actions/profileActions';
import {ProfileAPI} from "../../components/Profile/ProfileAPI";
import {sessionSetAction} from "../actions/sessionAction";

export const profileMiddleware = ({dispatch}) => next => action => {
    next(action)

    if (action.type === PROFILE_TRANSLATION_ATTEMPTING) {
        console.log(action.payload)
        ProfileAPI.addTranslation(action.payload)
            .then((translations) => dispatch(translateSuccessAction(translations)))
            .catch(error => (
                dispatch(translateErrorAction(error.message))
            ))
    }

    if (action.type === PROFILE_TRANSLATION_SUCCESS) {
        dispatch(translateSuccessAction(action.payload))
    }
}
