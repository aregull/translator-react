import {loginReducer} from './loginReducer'
import {combineReducers} from "redux";
import {sessionReducer} from "./sessionReducer";
import {profileReducer} from "./profileReducer";

const appReducer = combineReducers({
    loginReducer,
    profileReducer,
    sessionReducer
})

export default appReducer