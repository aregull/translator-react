import {
    PROFILE_TRANSLATION_SUCCESS,
    PROFILE_TRANSLATION_ERROR,
    PROFILE_TRANSLATION_ATTEMPTING
} from "../actions/profileActions";

const initialState = {
   translateError: '',
}

export const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case PROFILE_TRANSLATION_ATTEMPTING:
            return {
            ...state,
                translateError: '',
            }
        case PROFILE_TRANSLATION_SUCCESS:
            return {
                ...initialState,
            }
        default:
            return state
    }
}